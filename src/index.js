import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import createHistory from 'history/createBrowserHistory';
import Dashboard from './components/dashboard';
import ImagesGrid from './components/gallery/images-grid';
import ImagePreview from './components/gallery/image-preview';
import ImagesGridAsync from './components/gallery/images-grid-async';
import ImagePreviewAsync from './components/gallery/image-preview-async';
import Playground from './components/playground/parent';
import './theme/scss/bootstrap.scss';
import './theme/scss/app.scss';

const history = createHistory();

const Root = (
    <Router history={history}>
        <div className="app">
            <Route exact={true} path="/" component={Dashboard} />
            <Route exact={true} path="/images" component={ImagesGrid} />
            <Route exact={true} path="/images/:imageId" component={ImagePreview} />
            <Route exact={true} path="/images-async" component={ImagesGridAsync} />
            <Route exact={true} path="/images-async/:imageId" component={ImagePreviewAsync} />
            <Route exact={true} path="/playground" component={Playground} />
        </div>
    </Router>
);

render(Root, document.getElementById('root'));
