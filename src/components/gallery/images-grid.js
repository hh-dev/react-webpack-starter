import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import images from '../../mock-data/images.json';

class ImagesGrid extends Component {
    constructor(props) {
        super(props);

        this.state = {
            images
        }
    }

    toggleLike(isLiked, ix) {
        const nextState = Object.assign({}, this.state);
        nextState.images[ix].isLiked = !nextState.images[ix].isLiked;

        this.setState(nextState);
    }

    goToImagePreview(id) {
        this.props.history.push(`/images/${id}`);
    }

    renderGridImage(image, ix) {
        const { id, src, title, isLiked } = image;

        return (
            <Col xs={12} sm={4} key={id} className="grid-image">
                <Card>
                    <CardHeader
                        title={title}
                        className="header"
                        action={
                            <IconButton onClick={() => this.toggleLike(isLiked, ix)}>
                                <FavoriteIcon className={`${isLiked ? 'is-liked' : ''}`} />
                            </IconButton>
                        }
                    />
                    <CardMedia
                        image={src}
                        title={title}
                        className="image"
                        onClick={() => this.goToImagePreview(id)}
                    />
                </Card>
            </Col>
        );
    }

    render() {
        const { images } = this.state;

        return (
            <Grid className="component--gallery--images-grid">
                <Row>
                    {images.map((image, ix) => this.renderGridImage(image, ix))}
                </Row>
            </Grid>
        );
    }
}

export default ImagesGrid;
