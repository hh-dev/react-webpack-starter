import React, { Component } from 'react';
import axios from 'axios';
import { Grid, Row, Col } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';

class ImagePreview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imageId: props.match.params.imageId,
            image: {}
        }
    }

    componentDidMount() {
        const { imageId } = this.state;
        this.fetchImage(imageId);
    }

    async fetchImage(id) {
        try {
            const accessKey = 'd9b44c21fd7ec3d947d0d627ec036d2ee3784b56f33c9d54a5fedb1ba18e90c0';
            const url = `https://api.unsplash.com/photos/${id}`;
            const options = {
                headers: {
                    Authorization: `Client-ID ${accessKey}`
                }
            };

            const response = await axios.get(url, options);
            const image = response.data;

            this.setState({ image });
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const id = _.get(this.state, 'imageId');
        const src = _.get(this.state, 'image.urls.regular');

        let element;

        if (id && src) {
            element = (
                <Card className="content">
                    <CardHeader title={id} className="header" />
                    <CardMedia
                        image={src}
                        title={id}
                        className="image"
                    />
                </Card>
            );
        }

        return (
            <Grid className="component--gallery--image-preview">
                <Row>
                    <Col xs={12}>
                        {element}
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default ImagePreview;
