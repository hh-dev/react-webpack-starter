import React, { Component } from 'react';
import axios from 'axios';
import { Grid, Row, Col } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';

class ImagesGrid extends Component {
    constructor(props) {
        super(props);

        this.state = {
            images: []
        }
    }

    componentDidMount() {
        this.fetchImages();
    }

    async fetchImages() {
        try {
            const accessKey = 'd9b44c21fd7ec3d947d0d627ec036d2ee3784b56f33c9d54a5fedb1ba18e90c0';
            const url = 'https://api.unsplash.com/photos?per_page=9';
            const options = {
                headers: {
                    Authorization: `Client-ID ${accessKey}`
                }
            };

            const response = await axios.get(url, options);
            const images = response.data;

            this.setState({ images });
        } catch (error) {
            console.log(error);
        }
    }

    toggleLike(isLiked, ix) {
        const nextState = Object.assign({}, this.state);
        nextState.images[ix].isLiked = !nextState.images[ix].isLiked;

        this.setState(nextState);
    }

    goToImagePreview(id) {
        this.props.history.push(`/images-async/${id}`);
    }

    renderGridImage(image, ix) {
        const { id, urls, isLiked } = image;

        return (
            <Col xs={12} sm={4} key={id} className="grid-image">
                <Card>
                    <CardHeader
                        title={id}
                        className="header"
                        action={
                            <IconButton onClick={() => this.toggleLike(isLiked, ix)}>
                                <FavoriteIcon className={`${isLiked ? 'is-liked' : ''}`} />
                            </IconButton>
                        }
                    />
                    <CardMedia
                        image={urls.regular}
                        title={id}
                        className="image"
                        onClick={() => this.goToImagePreview(id)}
                    />
                </Card>
            </Col>
        );
    }

    render() {
        const { images } = this.state;

        return (
            <Grid className="component--gallery--images-grid">
                <Row>
                    {images.map((image, ix) => this.renderGridImage(image, ix))}
                </Row>
            </Grid>
        );
    }
}

export default ImagesGrid;
