import _ from 'lodash';
import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import images from '../../mock-data/images.json';

class ImagePreview extends Component {
    constructor(props) {
        super(props);

        const { imageId } = props.match.params;
        const image = _.find(images, (o) => _.toString(o.id) === imageId);

        this.state = {
            image
        }
    }

    render() {
        const { src, title } = this.state.image;

        return (
            <Grid className="component--gallery--image-preview">
                <Row>
                    <Col xs={12}>
                        <Card className="content">
                            <CardHeader title={title} className="header" />
                            <CardMedia
                                image={src}
                                title={title}
                                className="image"
                            />
                        </Card>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default ImagePreview;
