import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Row, Col } from 'react-bootstrap';

class Dashboard extends Component {
    render() {
        return (
            <div className="dashboard">
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <h3>React + Webpack Demo</h3>
                            <ul>
                                <li><Link to="images">Gallery</Link></li>
                                <li><Link to="images-async">Gallery Async</Link></li>
                                <li><Link to="playground">Playground</Link></li>
                            </ul>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Dashboard;
