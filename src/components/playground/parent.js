import React, { Component } from 'react';
import Child from './child';

class Parent extends Component {
    constructor() {
        super();
        this.state = {
            checked: false
        };

        this.toggleChecked = this.toggleChecked.bind(this);
    }

    toggleChecked() {
        const { checked } = this.state;
        this.setState({ checked: !checked });
    }

    render() {
        const { checked } = this.state;
        const myProps = {
            colour: 'blue',
            bodyStyle: 'sedan',
            make: 'Ford',
            model: 'Focus',
            parts: ['door', 'wheel', 'windows'],
            year: 2019,
            checked,
            toggleChecked: this.toggleChecked
        };

        return <Child {...myProps} />;
    }
}

export default Parent;
