import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Button } from 'react-bootstrap';

class Child extends Component {
    renderParts() {
        const { parts } = this.props;

        return (
            <div>
                <h4>Parts:</h4>
                <ul>
                    {parts.map((part, ix) => <li key={ix}>{part}</li>)}
                </ul>
            </div>
        )
    }

    render() {
        const { colour, bodyStyle, make, model, year, checked, toggleChecked } = this.props;

        return (
            <Grid>
                <Row>
                    <Col xs={12}>
                        <div>Colour: {colour}</div>
                        <div>Body style: {bodyStyle}</div>
                        <div>Make: {make}</div>
                        <div>Model: {model}</div>
                        <div>Year: {year}</div>
                        {this.renderParts()}
                        <div>Checked: {_.toString(checked)}</div>
                        <Button bsStyle="primary" onClick={toggleChecked}>
                            Toggle Checked
                        </Button>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

Child.propTypes = {
    colour: PropTypes.string.isRequired,
    bodyStyle: PropTypes.string.isRequired,
    make: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    year: PropTypes.number.isRequired,
    parts: PropTypes.arrayOf(
        PropTypes.string.isRequired
    ).isRequired,
    checked: PropTypes.bool.isRequired,
    toggleChecked: PropTypes.func.isRequired
};

export default Child;
