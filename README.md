# React + Webpack starter

## Installing dependencies
* Ensure your OS has Node v10.x and npm 6.x installed

## Development
1. Run `npm start`
2. Navigate to `http://localhost:8080`
