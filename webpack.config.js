const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
    // Define the "entry" files for each bundle of scripts
    entry: {
        app: ['./src/index.js']
    },
    output: {
        publicPath: '/'
    },
    devServer: {
        // Fallback to index.html in the event that a requested resource cannot be found during routing
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                // Load and transpile all .js and .jsx file with babel,
                // which supports the latest ES6 syntax (eg. import/export)
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[name]_[local]_[hash:base64]',
                            sourceMap: true,
                            minimize: true
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' }
                ]
            },
            {
                test: /(\.eot$|\.ttf$|\.woff$|\.woff2$)/,
                loader: 'file-loader?name=fonts/[name].[hash].[ext]'
            },
            {
                test: /(\.jpg$|\.png$|.svg$)/,
                loader: 'file-loader?name=images/[name].[hash].[ext]'
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html'
        })
    ]
};
